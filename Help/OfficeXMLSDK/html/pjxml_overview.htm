<html dir="ltr"><head>
<META http-equiv="Content-Type" content="text/html; charset=UTF-8">
<META HTTP-EQUIV="assetid" CONTENT="HV01051036"><META NAME="lcid" CONTENT="1033"><title>Project Schema Overview</title><link rel="stylesheet" type="text/css" href="office10.css"><script type="text/javascript" language="Javascript" src="ExpCollapse.js"></script><script type="text/javascript" language="JavaScript" src="inline.js"></script></head><body><h1>Project Schema Overview</h1><p>This reference provides the information necessary to understand the structure and individual elements of the 
	Microsoft Office Project 2003 XML Schema. In addition to descriptions for each element within the schema, 
	the overall structure of the schema is detailed, and some sample macros are included that show how to use the 
	XML Document Object Model (DOM) to access the Project XML Schema.</p><p><b>Note</b>  To ensure that all required elements and default settings are included when 
	you create a project in XML, it is recommended that you start by saving an empty project to 
	XML that you can use as a template.</p><h2>Appending XML Data to Existing Projects</h2><p>When Project appends XML data to an existing project, a summary task's unique ID 
	is incremented from 0 to 1. Project then checks for duplicate, unique IDs elsewhere in the 
	appended XML data; if any are found, they are given new, unique IDs. This process 
	can cause assignments, tasks, resources, and cross-project links to become disassociated with original 
	data.</p><p><b>Note</b>  To ensure that data doesn't end up in unintended locations, it is recommended that 
	you separate this data into its smallest possible component: a task, a resource, an assignment, and so on, 
	before appending. You can also create a custom solution by using Project Visual Basic for Applications (VBA) and MSXML. 
	See Project VBA Help for more information.</p><p>For examples of using VBA in Project to open and save XML data, see 
	<a href="pjxml_using.htm" id="HV01051041" lcid=" ">Using XML Files with Project</a>.
	</p><h2>Project XML Element and Data Types</h2><p>The following XML Schema Definition (XSD) element types are used in the Project XML Schema:</p><table>
			<tr><th>Element Types</th><th>Description</th></tr>
			<tr><td>schema</td><td>Contains the definition of the schema.</td></tr>
			<tr><td>annotation</td><td>Defines an annotation.</td></tr>
			<tr><td>complexType</td><td>A type definition for elements that contains elements and attributes. 
This data can contain elements and attributes.</td></tr>
			<tr><td>documentation</td><td>Specifies information to be read or used by users within the annotation element.</td></tr>
			<tr><td>sequence</td><td>Requires the elements in the group to appear in the specified sequence within the containing element.</td></tr>
			<tr><td>element</td><td>Declares an element.</td></tr>
			<tr><td>simpleType</td><td>A type definition for a value that can be used as the content of an element or attribute. 
This data type cannot contain elements or have attributes.</td></tr>
			<tr><td>restriction</td><td>Defines constraints on a simpleType definition.</td></tr>
			<tr><td>enumeration</td><td>A specified set of values for an element. Data is constrained to the specific values described.</td></tr>
		</table><p>The following data types are used in the Project XML Schema:</p><table>
			<tr><th>Data Types</th><th>Description</th></tr>
			<tr><td>integer</td><td>A sequence of decimal digits with an optional leading sign (+ or -). This data type is derived from decimal.</td></tr>
			<tr><td>float</td><td>A single-precision, 32-bit floating point number.</td></tr>
			<tr><td>string</td><td>A character string.</td></tr>
			<tr><td>datetime</td><td>A specific instance of time.</td></tr>
			<tr><td>time</td><td>An instance of time that recurs every day.</td></tr>
			<tr><td>duration</td><td>A duration of time.</td></tr>
			<tr><td>Boolean</td><td>A Boolean value, which is either True or False.</td></tr>
			<tr><td>decimal</td><td>An arbitrary precision number.</td></tr>
			<tr><td>TimephasedDataType</td><td>A complex data type that contains information about a task, resource, or assignment 
that is distributed over time. The &lt;TimephasedData&gt; element is of this type.</td></tr>
		</table><h2>The Project XML Schema</h2><p>The topics in this XML Schema reference explain each individual element of the Project XML Schema. 
The schema is broken into sections by concept of data in relation to how it exists 
in Project, Microsoft Office Project Server 2003, and the OLE databases, 
rather than being based on the literal organization of the actual schema itself. For example:</p><pre><code>&lt;xsd:element name="Project"&gt;
   &lt;xsd:complexType&gt;
      &lt;xsd:sequence&gt;
         &lt;xsd:element name="UID" minOccurs="0"&gt;
            &lt;xsd:simpleType&gt;
               &lt;xsd:restriction base="xsd:string"&gt;
                  &lt;xsd:maxLength value="16" /&gt;
               &lt;/xsd:restriction&gt;
            &lt;/xsd:simpleType&gt;
         &lt;/xsd:element&gt;
         &lt;xsd:element name="CreationDate"&gt;
         &lt;/xsd:element&gt;
      &lt;/xsd:sequence&gt;
   &lt;/xsd:complexType&gt;
&lt;/xsd:element&gt;</code>
		</pre><p>The elements in the example shown above are included in the topic 
<a href="pjxml_structProject.htm" id="HV01050603" lcid=" ">XML Structure for the &lt;Project&gt; Element</a>. 
The &lt;UID&gt; and &lt;CreationDate&gt; elements each have their own topic descriptions, 
which may be referenced from the XML Structure list or from the 
<a href="pjxml_list.htm" id="HV01051034" lcid=" ">Alphabetical List of Elements and Types</a>.</p><p>The &lt;UID&gt; element is a child not only of &lt;Project&gt;, but also of several other complex types, such as
&lt;Calendar&gt;, &lt;Resource&gt;, &lt;Assignment&gt;, and &lt;Task&gt;. The topic description of each 
element includes a list of its parent elements.</p><p>Here is another, more complex example:</p><pre><code>&lt;xsd:element name="Calendars"&gt;
...
   &lt;xsd:element name="Weekday"&gt;
      &lt;xsd:complexType&gt;
         &lt;xsd:sequence&gt;
            &lt;xsd:element name="DayWorking" minOccurs="0"&gt;
            &lt;xsd:element name="DayType" minOccurs="0"&gt;
            ...
            &lt;xsd:element name="WorkingTimes" minOccurs="0"&gt;
            ...
               &lt;xsd:complexType&gt;
                  &lt;xsd:sequence&gt;
                     &lt;xsd:choice&gt;
                        &lt;xsd:element name="WorkingTime" minOccurs="0" maxOccurs="5"&gt;
                        ...
                           &lt;xsd:element name="FromTime" minOccurs="0"&gt;
                           &lt;xsd:element name="ToTime" minOccurs="0"&gt;
            ...
         &lt;/xsd:sequence&gt;
      &lt;/xsd:complexType&gt;
   &lt;/xsd:element&gt;
...
&lt;/xsd:element&gt;</code>
		</pre><p>Links to the element topics in the example shown above are included in the topic 
<a href="pjxml_structCalendar.htm" id="HV01051054" lcid=" ">XML Structure for the &lt;Calendar&gt; Element</a>
, and in <a href="pjxml_list.htm" id="HV01051034" lcid=" ">Alphabetical List of Elements and Types</a>. 
The &lt;WorkingTimes&gt; element (note the plural form) is a collection of the singular 
&lt;WorkingTime&gt; element, which is defined by &lt;FromTime&gt; and &lt;ToTime&gt;.  In general, it is easier to see the parent-child relationships 
in the XML Structure topics, rather than in the schema itself.</p><center><a href="XMLSchemaCopyright_HV01147162.htm">&copy;2003-2004 Microsoft Corporation. All rights reserved.</a>  
Permission to copy, display and distribute this document is available at: <a 
href="http://r.office.microsoft.com/r/rlidAWSContentRedir?AssetID=XT010988631033&amp;CTT=11&amp;Origin=HV011232471033" 
target="_new">http://msdn.microsoft.com/library/en-us/odcXMLRef/html/odcXMLRefLegalNotice.asp</a></center></body></html>